package casir.matchgame;

import java.lang.Exception;
import java.util.Scanner;
import java.util.Random;

public class MatchGame {

    public static void main(String[] args){
        int remaining = 10;
        while (true) {
            Scanner reader = new Scanner(System.in);
            System.out.println("Enter a number between 1 and 3: ");
            int n = reader.nextInt();
            remaining -= n;
            if (remaining == 1){
                System.out.println("You Win");
                break;
            }
            System.out.println("Remaining " + remaining);
            n = new Random().nextInt(2) + 1;
            remaining -= n;
            if (remaining == 1){
                System.out.println("You Loose");
                break;
            }
            System.out.println("Remaining " + remaining);
        }
    }

    public static int add(int a, int b) throws Exception {
        int c = a + b;
        if (c == 0){
            throw new Exception("c is zeroooo !");
        }
        return c;
    }
}